class SignUp {

    get login_menu (){
        return $('~Login')
    }

    get login_main_title (){
        return $('//android.widget.ScrollView[@content-desc="Login-screen"]')
    }

    get login_sub_title (){
        return $('//android.view.ViewGroup[@content-desc="button-login-container"]')
    }

    get signup_sub_title (){
        return $('//android.view.ViewGroup[@content-desc="button-sign-up-container"]')
    }

    get email_text_field (){
        return $('~input-email')
    }

    get password_text_field (){
        return $('~input-password')
    }

    get confirm_password_text_field (){
        return $('~input-repeat-password')
    }
    get email_error_message (){
        return $('//android.widget.TextView[@index=1]')
    }
    get password_error_message (){
        return $('//android.widget.TextView[@index=3]')
    }
    get confirm_password_error_message (){
        return $('//android.widget.TextView[@index=5]')
    }
    get signup_button (){
        return $('~button-SIGN UP')
    }

    get success_popup_alert (){
        return $('//android.widget.TextView[@text="Signed Up!"]')
    }

    get success_popup_desc (){
        return $('//android.widget.TextView[@text="You successfully signed up!"]')
    }

    get success_popup_button (){
        return $('//android.widget.Button[@text="OK"]')
    }
}
module.exports = new SignUp();
