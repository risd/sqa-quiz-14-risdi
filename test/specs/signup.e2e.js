import SignUp from '../pageobjects/signup.page.js'

describe('Sign Up', () => {

    it('sign up with blank email, password, and confirm password fields should display an error', async () => {
        await SignUp.login_menu.click()
        await SignUp.signup_sub_title.click()
        await SignUp.email_text_field.setValue('')
        await SignUp.password_text_field.setValue('')
        await SignUp.confirm_password_text_field.setValue('')
        await SignUp.signup_button.click()
        await expect(SignUp.email_error_message).toHaveText('Please enter a valid email address')
        await expect(SignUp.password_error_message).toHaveText('Please enter at least 8 characters')
        await expect(SignUp.confirm_password_error_message).toHaveText('Please enter the same password')
    });

    it('sign up with blank email field should display an error', async () => {
        // await SignUp.login_menu.click()
        // await SignUp.signup_sub_title.click()
        await SignUp.email_text_field.setValue('')
        await SignUp.password_text_field.setValue('Akuntes1')
        await SignUp.confirm_password_text_field.setValue('Akuntes1')
        await SignUp.signup_button.click()
        await expect(SignUp.email_error_message).toHaveText('Please enter a valid email address')
        await expect(SignUp.password_error_message).toHaveText('')
        await expect(SignUp.confirm_password_error_message).toHaveText('')
    });

    it('sign up with blank password field should display an error', async () => {
        // await SignUp.login_menu.click()
        // await SignUp.signup_sub_title.click()
        await SignUp.email_text_field.setValue('jormugnandr@mailinator.com')
        await SignUp.password_text_field.setValue('')
        await SignUp.confirm_password_text_field.setValue('Akuntes1')
        await SignUp.signup_button.click()
        await expect(SignUp.email_error_message).toHaveText('')
        await expect(SignUp.password_error_message).toHaveText('Please enter at least 8 characters')
        await expect(SignUp.confirm_password_error_message).toHaveText('Please enter the same password')
    });

    it('sign up with blank confirm password field should display an error', async () => {
        // await SignUp.login_menu.click()
        // await SignUp.signup_sub_title.click()
        await SignUp.email_text_field.setValue('jormugnandr@mailinator.com')
        await SignUp.password_text_field.setValue('Akuntes1')
        await SignUp.confirm_password_text_field.setValue('')
        await SignUp.signup_button.click()
        await expect(SignUp.email_error_message).toHaveText('')
        await expect(SignUp.password_error_message).toHaveText('')
        await expect(SignUp.confirm_password_error_message).toHaveText('Please enter the same password')
    });

    it('sign up with an invalid email format should display an error', async () => {
        // await SignUp.login_menu.click()
        // await SignUp.signup_sub_title.click()
        await SignUp.email_text_field.setValue('jormugnandr')
        await SignUp.password_text_field.setValue('Akuntes1')
        await SignUp.confirm_password_text_field.setValue('Akuntes1')
        await SignUp.signup_button.click()
        await expect(SignUp.email_error_message).toHaveText('Please enter a valid email address')
        await expect(SignUp.password_error_message).toHaveText('')
        await expect(SignUp.confirm_password_error_message).toHaveText('')
    });

    it('sign up with less than 8 characters of password should display an error', async () => {
        // await SignUp.login_menu.click()
        // await SignUp.signup_sub_title.click()
        await SignUp.email_text_field.setValue('jormugnandr@mailinator.com')
        await SignUp.password_text_field.setValue('Akuntes')
        await SignUp.confirm_password_text_field.setValue('Akuntes')
        await SignUp.signup_button.click()
        await expect(SignUp.email_error_message).toHaveText('')
        await expect(SignUp.password_error_message).toHaveText('Please enter at least 8 characters')
        await expect(SignUp.confirm_password_error_message).toHaveText('Please enter the same password')
    });

    it('sign up with a different password should display an error', async () => {
        // await SignUp.login_menu.click()
        // await SignUp.signup_sub_title.click()
        await SignUp.email_text_field.setValue('jormugnandr@mailinator.com')
        await SignUp.password_text_field.setValue('Akuntes1')
        await SignUp.confirm_password_text_field.setValue('Akuntes2')
        await SignUp.signup_button.click()
        await expect(SignUp.email_error_message).toHaveText('')
        await expect(SignUp.password_error_message).toHaveText('')
        await expect(SignUp.confirm_password_error_message).toHaveText('Please enter the same password')
    });

    it('sign up with valid data should display success popup', async () => {
        // await SignUp.login_menu.click()
        // await SignUp.signup_sub_title.click()
        await SignUp.email_text_field.setValue('jormugnandr@mailinator.com')
        await SignUp.password_text_field.setValue('Akuntes1')
        await SignUp.confirm_password_text_field.setValue('Akuntes1')
        await SignUp.signup_button.click()
        await browser.pause(3000)
        await expect(SignUp.success_popup_alert).toBeDisplayed()
        await expect(SignUp.success_popup_alert).toHaveText('Signed Up!')
        await expect(SignUp.success_popup_desc).toBeDisplayed()
        await expect(SignUp.success_popup_desc).toHaveText('You successfully signed up!')
        await expect(SignUp.success_popup_button).toBeDisplayed()
        await expect(SignUp.success_popup_button).toHaveText('OK')
    });

});
