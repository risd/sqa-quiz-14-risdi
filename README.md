1. Start your android emulator and **make sure you have the** [wdio demo app](https://github.com/webdriverio/native-demo-app/releases) **installed**
2. Clone this repo `git clone https://gitlab.com/risd/sqa-quiz-14-risdi.git`
3. Open terminal inside project folder and run `npm install`
4. Start appium server `appium` 
   Just in case appium doesn't find the uiautomator2 driver, install it with `appium driver install uiautomator2`
5. Open new terminal and run test with `npx wdio run ./wdio.conf.js`
6. Generate test results with allure

- install allure `npm install @wdio/allure-reporter --save-dev` 
- install allure commandline `npm install allure-commandline --save-dev`
- generate test results `npx allure generate allure-results --clean` 
- Open test report `npx allure open`
